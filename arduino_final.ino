#include <LiquidCrystal.h>
LiquidCrystal lcd(3, 4, 6, 7, 8, 9);

#include <AddicoreRFID.h>
#include <SPI.h>

#define  uchar unsigned char
#define uint  unsigned int

uchar fifobytes;
uchar fifoValue;

AddicoreRFID myRFID; // create AddicoreRFID object to control the RFID module

/////////////////////////////////////////////////////////////////////
//set the pins
/////////////////////////////////////////////////////////////////////
const int chipSelectPin = 10;
const int NRSTPD = 5;

//Maximum length of the array
#define MAX_LEN 16


String incoming;

int reset;

int MoveX;
int MoveY;

int anX;
int anY;

int fullReload = 2;

int count1 = fullReload, count2 = fullReload, count3 = fullReload, count4 = 0;

int fullAmmo = 16;

int ammo1 = fullAmmo, ammo2= fullAmmo, ammo3= fullAmmo, ammo4= 4;

int current_ammo = 1;

bool shot = false;

String ToUnity;

float tiempo = 0;

String key;

void setup() {
  Serial.begin(9600);

  // start the SPI library:
  SPI.begin();
  
  pinMode(chipSelectPin,OUTPUT);              // Set digital pin 10 as OUTPUT to connect it to the RFID /ENABLE pin 
    digitalWrite(chipSelectPin, LOW);         // Activate the RFID reader
  pinMode(NRSTPD,OUTPUT);                     // Set digital pin 10 , Not Reset and Power-down
    digitalWrite(NRSTPD, HIGH);

  myRFID.AddicoreRFID_Init();
  
  lcd.begin(16,2);
  pinMode(2, INPUT);
  pinMode(A5, INPUT);
  pinMode(A4, OUTPUT);
  digitalWrite(A4, LOW);
  delay(100);
}
void loop() {

  ToUnity = "";

  ProcessReset();

  ProcessUltraSonido();

  ProcessRFID();

  ProcessJoystick();

  Serial.println(ToUnity);

  ProcessLCD();

}

void ProcessReset(){
  reset = digitalRead(2);

  if(reset == HIGH || FromUnity())
  {
    count1 = fullReload;
    count2 = fullReload;
    count3 = fullReload;
    count4 = 0;

    ammo1 = fullAmmo;
    ammo2 = fullAmmo;
    ammo3 = fullAmmo;
    ammo4 = 4;

    current_ammo = 1;

    ToUnity += "1";
  }
  else
    ToUnity += "0";
}

bool FromUnity(){
  if (Serial.available() > 0) {
    incoming = Serial.read();

    if (incoming == "102")
      return true;
    else
      return false;
  }
  else
    return false;
}

void ProcessUltraSonido(){
  
  digitalWrite(A4, HIGH);
  delayMicroseconds(10);
  digitalWrite(A4, LOW);
  tiempo = pulseIn(A5, HIGH);
  
  if(tiempo < 1000)
  {
    shot = true;
    ToUnity += "1";
  }
  else
    ToUnity += "0";
    
}

void ProcessRFID(){
  
      uchar i, tmp, checksum1;
  uchar status;
        uchar str[MAX_LEN];
        uchar RC_size;
        uchar blockAddr;  //Selection operation block address 0 to 63
        String mynum = "";

        str[1] = 0x4400;
  //Find tags
  status = myRFID.AddicoreRFID_Request(PICC_REQIDL, str); 

  //Anti-collision, return tag serial number 4 bytes
  status = myRFID.AddicoreRFID_Anticoll(str);
  if (status == MI_OK)
  {
          key = "";
          checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3];
          
          key += str[0];
          key += str[1];
          key += str[2];
          key += str[3];
         
          if (str[4] == checksum1)
          {
            if(key == "1158646")
            {
                current_ammo = 1;
            }
            else if(key == "4825423798")
            {
                current_ammo = 2;
            }
            else if(key == "29239238208")
            {
                current_ammo = 3;
            }
            else if(key == "138213139121")
            {
                current_ammo = 4;
            }
          }
  }
    
        myRFID.AddicoreRFID_Halt();
        
  ToUnity += current_ammo;
}
void ProcessJoystick(){
  anX = analogRead(A0);
  anY = analogRead(A1);

  if (anX > 800)
  {
    ToUnity += "2";
  }
  else if (anX < 200)
  {
    ToUnity += "0";
  }
  else
  {
    ToUnity += "1";
  }

  if (anY > 800)
  {
    ToUnity += "2";
  }
  else if (anY < 200)
  {
    ToUnity += "0";
  }
  else
  {
    ToUnity += "1";
  }
}
void ProcessLCD(){

  for (int i = 0; i < 24; i++)
  {
    lcd.setCursor(i,0);
    lcd.print(" ");
    lcd.setCursor(i,1);
    lcd.print(" ");
  }

  lcd.setCursor(0,0);
  lcd.print("Tipo: ");
  lcd.print(current_ammo);

  if (current_ammo == 1){
    if(shot) ammo1 -=1;

    if(ammo1 < 1 and count1 > 0)
    {
      ammo1 = fullAmmo;
      count1 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count1);
    Display_Bullets('I', ammo1);
  }
  else if (current_ammo == 2){
    if(shot) ammo2 -=1;

    if(ammo2 < 1 and count2 > 0)
    {
      ammo2 = fullAmmo;
      count2 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count2);
    Display_Bullets('O', ammo2);
  }
  else if (current_ammo == 3){
    if(shot) ammo3 -=1;

    if(ammo3 < 1 and count3 > 0)
    {
      ammo3 = fullAmmo;
      count3 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count3);
    Display_Bullets('Y', ammo3);
  }
  else if (current_ammo == 4){
    if(shot) ammo4 -=1;

    if(ammo4 < 1 and count4 > 0)
    {
      ammo4 = fullAmmo;
      count4 -= 1;
    }
    lcd.print("    R: ");
    lcd.print(count4);
    Display_Bullets('W', ammo4);
  }
  
  shot = false;
  delay(100);
}

void Display_Bullets(char ct, int ammount){

  for(int i = 0; i < ammount; i++)
  {
    lcd.setCursor(i,1);
    lcd.print(ct);
  }
}
